<?php
error_reporting(0);
ini_set("user_agent","Manivela Bot - https://gitorious.org/manivela");
ini_set("default_charset",'UTF-8');
date_default_timezone_set("UTC");
$progname = "Manivela";
$condir = $_SERVER['HOME']."/.".strtolower($progname);
#COLUMNAS BASE DE DATOS USUARIO
$colsu = array(
	'pumpid',
	'token',
	'token_secret',
	'access'
);
#COLUMNAS BASE DE DATOS NODOS
$colsh = array(
	'host',
	'consumer_key',
	'consumer_secret',
	'secure'
);
#COLUMNAS BASE DE DATOS NODOS
$colsc = array(
	'cuenta_primaria',
	'to_pred',
	'cc_pred',
	'bto_pred',
	'bcc_pred'
);
#ARCHIVOS DONDE GUARDAR CONFIGURACION
$aru = $condir.'/usuarios';
$arh = $condir.'/nodos';
$arc = $condir.'/configuracion';
#DATOS CLIENTE
$cliente = array(
		'type' => 'client_associate',
		'contacts' => 'sotitrox@autistici.org',
		'application_type' => 'web',
		'application_name' => 'Manivela'
);
?>