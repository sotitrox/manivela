<?php
class manivela {
	function compBase($dir, $archivo) {
		if(!file_exists($dir)) {
			if(!mkdir($dir, 0700, true)) {
				return false;
			}
		}
		if(!is_dir($dir)) {
			return false;
		}
		if(!textBase::comprobarBase($archivo)) {
			return false;
		}
		return true;
	}
}
?>