<?php
class textBase {
	function comprobarBase($file) {
		$fp = fopen($file, "r");
		fclose($fp);
		if(!$fp) {
			$fp = fopen($file, "a");
			fclose($fp);
			if(!$fp) {
				return false;
			}
			else {
				return true;
			}
		}
		else {
			return true;
		}
	}
	function writeRow($file, array $cols, array $row) {
		#ordenar los datos
		$insert = array();
		for($x=0;$x < count($cols);$x++) {
			$insert[$x] = $row[$cols[$x]];
		}
		$insert = implode('|', $insert);
		$fp = fopen($file, "a");
		$write = fputs($fp, $insert."\n");
		fclose($fp);
		if($write) {
			return true;
		}
		else {
			return false;
		}
	}
	function createRegexp(array $array) {
		$regexp = array();
		for($x=0;$x < count($array);$x++) {
			if(strlen($array[$x])>0) {
				$regexp[$x] = $array[$x];
			}
			else {
				$regexp[$x] = '.*';
			}
		}
		return "/".implode('\|', $regexp)."/";
	}
	function search($file, array $where, array $cols) {
		$array_file = file($file);
		$read = array();
		for($x=0;$x < count($cols);$x++) {
			$read[$x] = $where[$cols[$x]];
		}
		$search = preg_grep(self::createRegexp($read),$array_file);
		if(count($search) == 0) {
			return false;
		}
		else {
			return $search;
		}
	}
	function selectWhere($file, array $where, array $select, array $cols) {
		$search = self::search($file, $where, $cols);
		$return = array();
		$search = array_values($search);
		for($x=0;$x<count($search);$x++) {
			$seleced = array();
			$current = explode('|',$search[$x]);
			for($y=0; $y < count($cols);$y++) {
				$current[$cols[$y]] = $current[$y];
			}
			for($y=0; $y < count($select);$y++) {
				$selected[$select[$y]] = trim($current[$select[$y]]);
			}
			$return[$x] = $selected;
		}
		return $return;
	}
	function updateWhere($file, array $where, array $update, array $cols) {
		$search = self::search($file, $where, $cols);
		if($search !== false) {
			$old = array();
			foreach($search as $key => $val) {
				$currentval = explode('|', $val);
				for($x=0;$x < count($cols);$x++) {
					$old[$key][$cols[$x]] = $currentval[$x];
				}
				$keys = array_keys($update);
				$values = array_values($update);
				for($x=0;$x < count($update);$x++) {
					$old[$key][$keys[$x]] = $values[$x];
				}
				$new[$key] = trim(implode('|', $old[$key]))."\n";
			}
			$array_file = file($file);
			$new = array_replace($array_file, $new);
			$newfile = implode('', $new);
			file_put_contents($file, $newfile, $flags = null, $context = null);
			return true;
		}
		else {
			return true;
		}
	}
	function deleteWhere($file, array $where, array $cols) {
		$search = self::search($file, $where, $cols);
		if($search !== false) {
			$array_file = file($file);
			$keys = array_keys($search);
			foreach($keys as $val) {
				$array_file[$val] = false;
			}
			$new = array_filter($array_file);
			$newfile = implode('', $new);
			file_put_contents($file, $newfile, $flags = null, $context = null);
			return true;
		}
		else {
			return true;
		}
	}
}
?>